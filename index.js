var vow = require('vow'),
request = require('request'),
printObj = function(argument){
	for (prop in argument) {
		if (argument.hasOwnProperty(prop)) {
			console.log(prop);
		}
	}
},
makeCallback = function(name){
	return function (argument) {
		console.log('Callback:\n' + name);
		// printObj(argument);

	}
};


var url = 'http://localhost:3300/';

function doSomethingAsync(url, cbk) {
    var defer = vow.defer(),
	noop = function() {},
	callback = function(res, body) {
	    defer.resolve(body);
	},
	req = request(url, callback);
	req.on('complete', callback);
    return defer.promise().then(cbk);
};
doSomethingAsync(url, function(body) {
	console.log(body);
});
// doSomethingAsync(url).then(function(body) {
// 	console.log(body);
// });
