var cheerio = require('cheerio'),
	request = require('request'),
	HttpsAgent = require('socks5-https-client/lib/Agent'),
	HttpAgent = require('socks5-http-client/lib/Agent'),
	vow = require('vow'),
	log = require('./log'),
	url = require('url'),
	testUrl = 'https://check.torproject.org/';

var setAgent = function (target) {
	var protocol = url.parse(target).protocol, agent;
	if (protocol == 'https:') {
		agent = HttpsAgent
	} else if (protocol == 'http:') {
		agent = HttpAgent
	} else {
		console.log('No valid protocol:', target);
	};
	log('protocol:', protocol);
	return agent;
};

var grab = function(target) {
	var defer = vow.defer(),
		reqOptions = {
		    url: target,
		    agentClass: setAgent(target),
		    agentOptions: { socksPort: 9050 },
			noop: function() {}
		},
		req = request(reqOptions, reqOptions.noop);
		req.on('complete', function(res, body) {
			defer.resolve(cheerio.load(body));
		});
    	return defer.promise();
};


grab.check = function() {
	grab(testUrl).then(function($) {
		console.log( $.html('title') );
	});
};

module.exports = grab;