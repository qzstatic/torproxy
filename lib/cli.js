/**
 * CLI
 * ===
 *
 * Этот файл запускается из командной строки
 */

var pkg = require('../package.json'),
	version = pkg.version,
	program = require('commander'),
	url = require('url'),
	log = require('./log'),
	torpoxy = require('./../torproxy.js');

program.version(version)
		.usage('[URL]...[jQuery selector]')
    	.parse(process.argv);

if (!program.args.length) {
    program.outputHelp();
}

program.command('*')
    .action(function () {
        var args = program.args.slice(0),
        	target = args[0],
        	selector = args[1];

		torpoxy(target, selector);
    });

program.command('check')
	.action(function() {
		torpoxy.check()
	});

program.parse(process.argv);
