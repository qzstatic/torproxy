var grab = require('./lib/grab');

torpoxy = function(target, selector) {
	return grab(target).then(function($) {
		console.log( $.html(selector) );
	});
};

torpoxy.grab = grab;
torpoxy.check = grab.check;

module.exports = torpoxy;