CLI

Usage: torproxy [URL]...[jQuery selector]

Options:

-h, --help     output usage information
-V, --version  output the version number

NodeJs

var torproxy = require('torproxy');
var url = 'https://example.com'
torpoxy.check()


torproxy(url, '#css_id')
torproxy(url, '.css_class')
torproxy(url, selector)

torproxy.grab(url, function ($, res) {
	console.log($(selector));
	console.log(res.body);
});
